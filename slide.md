---
title: SLIDE TITLE
theme: UNUSED (04/19/19)
---

![NoteRiot](https://note.riot.ai/static/img/icons/app-icon-noteriot-512x512.png)



<i class="mdi mdi-twitter"></i>@dantrevino

---

## Slide 2 ##

Markdown Content Goes Here

Note:

1) Presenter Notes Go Here

2) Presenter Notes Go Here

3) Presenter Notes Go Here

---

## Slide 3 ##

Markdown Content Goes Here


Note:

1) Presenter Notes Go Here

2) Presenter Notes Go Here

3) Presenter Notes Go Here

---

## Code! ##

```bash
npm install -g yo generator-blockstack
mkdir hello-blockstack && cd $_
yo blockstack
npm run start
```

Note:

1) Install Yeoman and the blockstack app generator using npm.

2) Create a new directory and `cd` into it.

3) Generate your Blockstack app.

4) Start the development server.

---

## Find Us ##

<i class="mdi mdi-chat"></i> forum.blockstack.org

<i class="mdi mdi-twitter"></i> @Blockstack

<i class="mdi mdi-github"></i> github.com/blockstack/

<i class="mdi mdi-chat"></i> Blockstack-Portland

Note: